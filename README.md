# About the Project
## The project to design a CICD pipeline to auto provision the AWS infrastructure as Code using Terraform
*  Terraform configuration file (*.tf) is stored on GitLab Repo
*  Terraform uses state files to store details about the infrastructure configuration. In this project, I use GitLab as Terraform HTTP backend to securely store the state file. Note that, GitLab supports state locking to to prevents others from acquiring the lock and potentially corrupting your state

## Prerequisite
* To interact with your AWS account, the GitLab CI/CD pipelines require both AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY to be defined in your GitLab project settings. You can do this under Settings > CI/CD > Variables.

## The CICD Pipeline
* CI/CD Pipeline: GitLab Repo ----[trigger]---> Gitlab Runner(default)---[pull]-->Terraform(image)---[deploy]---> AWS Cloud

  ![image](/uploads/0d6488ff265823b9b9a7428b3589973e/image.png)

* Pipeline stages
  - Validate: validates the Terraform configuration files pulled from GitLab repo
  - Plan: creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure
  - Apply: executes the actions proposed in the plan stage.
  - Destroy: destroy all AWS resources managed by the Terraform configuration

  ![image](/uploads/4526de53d9830576288cf48738ae5ae8/image.png)

## Notification
* To monitor the events for the pipelines, go to Project > Settings > Integrations, then add Slack notification

  ![image](/uploads/aecf361ce69f5485d883ad90bdc51024/image.png)

* GitLab will send the notification to Slack when git commit, push,... or pipeline status changes

  ![image](/uploads/451a569dc8242d2b173ab94c7c3631e9/image.png)
