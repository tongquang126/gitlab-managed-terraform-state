# List the required provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}
provider "aws" {
  region  = "us-east-1"
}

variable "workspace" {
  type = map(any)
  default = {
    default = "t2.micro"
    dev     = "t2.nano"
  }
}



resource "aws_instance" "foo" {
  ami           = "ami-0a8b4cd432b1c3063" # us-west-2
  instance_type = lookup(var.workspace, terraform.workspace, "default")
}
